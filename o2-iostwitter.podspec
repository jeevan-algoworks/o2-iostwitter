Pod::Spec.new do |s|
  s.name     = 'o2-iostwitter'

  s.license  = 'MIT'
  s.summary  = 'used for getting access token from twitter'
  s.homepage = 'https://bitbucket.org/mobicules'
  s.source   = { :git => 'https://bitbucket.org/jeevan-algoworks/o2-iostwitter'}
  s.source_files = 'O2iOSTwitter'

  s.requires_arc = true

  s.platform     = :ios, '7.0'

  s.dependency  'STTwitter', '~> 0.1' 



end