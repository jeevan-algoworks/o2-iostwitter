//
//  O2iOSTwitter.m
//  O2iOSTwitter
//
//  Created by Jeevan on 9/9/14.
//  Copyright (c) 2014 AlgoWorks. All rights reserved.
//


NSString  *consumerKey = @"q4twwNY9vDQYncEZ0RWB1Rdl7";
NSString  *secret = @"yxG6EX9FrdAsNr4STTxWDSBT8w6vTIwWN5B3wm6vLvICzqw4wc";


#import "O2iOSTwitter.h"
#import <UIKit/UIKit.h>

@interface O2iOSTwitter()

@property (nonatomic, strong) STTwitterAPI *twitter;

@end

@implementation O2iOSTwitter

/*
- (IBAction)loginWithiOSAction:(id)sender {
  
  self.twitter = [STTwitterAPI twitterAPIOSWithFirstAccount];
  
  _loginStatusLabel.text = @"Trying to login with iOS...";
  _loginStatusLabel.text = @"";
  
  [_twitter verifyCredentialsWithSuccessBlock:^(NSString *username) {
    
    _loginStatusLabel.text = username;
    
  } errorBlock:^(NSError *error) {
    _loginStatusLabel.text = [error localizedDescription];
  }];
  
} */

- (void)loginToTwitter {
  
  self.twitter = [STTwitterAPI twitterAPIWithOAuthConsumerKey:consumerKey
                                               consumerSecret:secret];
  
  
  [_twitter postTokenRequest:^(NSURL *url, NSString *oauthToken) {
    NSLog(@"-- url: %@", url);
    NSLog(@"-- oauthToken: %@", oauthToken);
    
    [[UIApplication sharedApplication] openURL:url];
  } authenticateInsteadOfAuthorize:NO
                  forceLogin:@(YES)
                  screenName:nil
               oauthCallback:@"myapp://twitter_access_tokens/"
                  errorBlock:^(NSError *error) {
                    NSLog(@"-- error: %@", error);
                    
                  }];
}


- (void)setOAuthToken:(NSString *)token oauthVerifier:(NSString *)verifier {
  
  [_twitter postAccessTokenRequestWithPIN:verifier successBlock:^(NSString *oauthToken, NSString *oauthTokenSecret, NSString *userID, NSString *screenName) {
    NSLog(@"token is %@",oauthToken);
    NSLog(@"secret is %@",oauthTokenSecret);
    
    NSLog(@"-- screenName: %@", screenName);
    
    
    
    /*
     At this point, the user can use the API and you can read his access tokens with:
     
     _twitter.oauthAccessToken;
     _twitter.oauthAccessTokenSecret;
     
     You can store these tokens (in user default, or in keychain) so that the user doesn't need to authenticate again on next launches.
     
     Next time, just instanciate STTwitter with the class method:
     
     +[STTwitterAPI twitterAPIWithOAuthConsumerKey:consumerSecret:oauthToken:oauthTokenSecret:]
     
     Don't forget to call the -[STTwitter verifyCredentialsWithSuccessBlock:errorBlock:] after that.
     */
    
  } errorBlock:^(NSError *error) {
    
    
    NSLog(@"-- %@", [error localizedDescription]);
  }];
}



@end
