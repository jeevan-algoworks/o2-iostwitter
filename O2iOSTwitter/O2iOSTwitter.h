//
//  O2iOSTwitter.h
//  O2iOSTwitter
//
//  Created by Jeevan on 9/9/14.
//  Copyright (c) 2014 AlgoWorks. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "STTwitter.h"

@interface O2iOSTwitter : NSObject

- (void)loginToTwitter ;

- (void)setOAuthToken:(NSString *)token oauthVerifier:(NSString *)verifier ;

@end
